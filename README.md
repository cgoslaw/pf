# Packet Filter, pf - OBSD
[Packet filter](https://www.openbsd.org/faq/pf/), commonly known as 'pf', is the default firewall for [OpenBSD](https://www.openbsd.org/) and is used in known softwares such as [pfSense](https://www.pfsense.org/) from [FreeBSD](https://www.freebsd.org/). pf has many features and advantages over many available firewalls, among these features are - Network Address Translation (NAT), Quality of Service (QoS), [pfsync](https://man.openbsd.org/pfsync) and [CARP protocol](https://man.openbsd.org/man4/carp.4), and [authpf](https://man.openbsd.org/man8/authpf.8).

Another interesting feature from pf is its logging that is configurable per rule within its configuration file [(pf.conf)](https://man.openbsd.org/pf.conf) and these logs are provided by a network interface named [pflog](https://man.openbsd.org/pflog). It's then possible to monitor logs in real time using utilities such as [tcpdump](https://man.openbsd.org/tcpdump.8), which was improved in OpenBSD, or saved to disk.

Since OpenBSD's main focus is security, a router setup running pf on OpenBSD can be considered one of the most secure solutions for someone looking for a router, allowing you to configure your rules by default blocking packets and then allowing what you need/want with a ruleset.
## Pre-Setup
This setup will be done in a virtual environment, with three subnets (192.168.0.0/24, 10.1.1.0/24 and 10.2.2.0/24) and several machines across these subnets with different services running on them such as DNS, Mail and LDAP servers.

A network segmentation like this is not needed and it will purely depend on what you want to achieve. You can also, of course, choose to set up a physical router and for that you clearly should have at least a NIC with multiple ports.

Also, for this guide, I won't be using [DHCP](https://man.openbsd.org/man8/dhcpd.8) for my machines, but you are free to do so.

## Setting up the network
Before we can start with pf, we have to setup the network interfaces by configuring [hostname.if(5)](https://man.openbsd.org/hostname.if.5). I can first check the list of my NICs by running [dmesg(8)](https://man.openbsd.org/dmesg):
```
# dmesg
<output omitted>
em0 at pci2 dev 1 function 0 "Intel 82545EM" rev 0x01: apic 1 int 19, address 00:0c:29:bb:86:08
em1 at pci2 dev 2 function 0 "Intel 82545EM" rev 0x01: apic 1 int 16, address 00:0c:29:bb:86:12
em2 at pci2 dev 3 function 0 "Intel 82545EM" rev 0x01: apic 1 int 17, address 00:0c:29:bb:86:1c
<output omitted>
```
From here I can see my three interfaces which in turn are going to be represented by a configuration file /etc/hostname.emX.

During the installation, you may have already set up the first interface that receives data directly from your ISP. I'll be setting them all nonetheless:
```
# echo 'inet 196.168.0.156 0xffffff00' > /etc/hostname.em0
# echo 'inet 10.1.1.156 0xffffff00' > /etc/hostname.em1
# echo 'inet 10.2.2.156 0xffffff00' > /etc/hostname.em2
```
Now all the interfaces have been assigned an IP address and a netmask.

If not configured previously during the installation, you may have to setup the IP of the ISP gateway in the [/etc/mygate](https://man.openbsd.org/mygate). Therefore it does also depend on your setup. In case you get your IP from the ISP modem via [DHCP](https://man.openbsd.org/dhcpd), there's no need for this file and it will be ignored.
```
# echo '192.168.0.1' > /etc/mygate
```
Lastly, I'll enable IP forwarding as to allow IP packets to travel between the network interfaces on the router. This can be done by modifying the kernel parameter 'net.inet.ip.forwarding' with [sysctl](https://man.openbsd.org/sysctl.8)
```
# sysctl net.inet.ip.forwarding=1
# echo 'net.inet.ip.forwarding=1' >> /etc/sysctl.conf
```
And now we are already able to forward IPv4 data between our NICs and interfaces.

Also, we can't forget to restart the network for the changes we've made to the network configuration, or restart the system if you prefer:
```
# sh /etc/netstart
# shutdown -r now
```
## pf Firewall
Before I get to configure the pf ruleset, let's try understand the most common rules/syntax. I suggest you to read [pf.conf(5)](https://man.openbsd.org/pf.conf.5), [pf(4)](https://man.openbsd.org/pf.4) and the [User's Guide](https://www.openbsd.org/faq/pf/) before you begin, as to understand the basics around it.

First of all, it's important to understand that [pf.conf(5)](https://man.openbsd.org/pf.conf.5) has an specific order of configuration that is, from first to last: macros and tables, options, translation, filtering and redirection.

### Common Settings - filters
When we set up our filtering ruleset, we either filter on the destination or on the source, e.g.,
```
<action> from src to dst [ports]
<action> from src [ports] to dst
```
The actions we are able to pass our filter are to pass, block or match packets.

A common recommended practice when setting up the firewall is to take the default deny approach, which is to deny everything with <block all> and then allow certain traffic to pass through the firewall. From there you may either specify a very specific set of rules, which may include a range of selected ports/services or mostly rely upon the security options while also having a simpler configuration. Either way is fine and it will, again, depend on what exactly you want to achieve for yourself.

**pass, block and match**
These are the actions you perform with filtering. pf has the ability to pass, block and match packets based on attributes. Filter rules will determine which of these actions will be taken upon matching the specified parameters that have been set.

Each time a packet processed by pf comes in or goes out an interface, the filter rules are evaluated in sequential order, from first to last. For block and pass, the last matching rule will decide what action is taken. For match, it provides fine grained filtering but without altering the block/pass state of a packet.

**in and out**
A packet always comes in on, or goes out through an interface. in and out apply to incoming and outgoing packets respectively. If neither are specified, a rule will end up matching packets going either way.

**quick**
If a packet matches a rule set with the quick option, the rule will be considered the last matching rule.

**on interface | any**
Applies to packet going either way through an specific interface or interface group. The any option will match any existing interface except for loopback interfaces.

**inet | inet6**
Determines the address family, either IPv4 (inet) or IPv6 (inet6).

**proto**
Applies to packets of only an specific protocol. The most common protocols are ICMP, UDP and TCP.

**from and to**
These modifiers apply only to packets with the specified src and dest addresses (and ports). The addresses can be specified in CIDR notation, symbolic host names, interface names or interface group names. All specifications are optional.

**nat-to**
Applies NAT, which modifies either the src or dst address and port of packets to outbound connections from inside the network to the Internet.

**rdr-to**
Applies rdr, which redirects packets from the Internet to an specific device in the internal network.

## Ruleset config
Now that most of the basic filter options are clear, we are ready to start configuring pf's configuration file:
```
# vim /etc/pf.conf
```
```
#	$OpenBSD: pf.conf,v 1.55 2017/12/03 20:40:04 sthen Exp $
#
# See pf.conf(5) and /etc/examples/pf.conf
#
# List edit: 24/11/2020

# ------------------------- #
# --- Macros and Tables --- #
# ------------------------- #
lo_if = "lo0"
main_if = "em0"
main_range = "192.168.0.0/24"
lan1_if = "em1"
lan1_range = "10.1.1.0/24"
lan2_if = "em2"
lan2_range = "10.2.2.0/24"
lan3_if = "em3"
lan3_range = "10.3.3.0/24"

un_srv = "10.1.1.127"
ns_srv = "{ 10.2.2.112, 10.2.2.224 }"
vt_srv = "{ 10.2.2.230, 10.2.2.231, 10.2.2.232 }"
ldap_srv = "{ 192.168.0.248 }"
mx01_srv = "{ 192.168.0.249 }"
void_ws = "{ 192.168.0.34 }"

icmp_types = "{ echoreq unreach }"

table <martians> { 0.0.0.0/8 10.0.0.0/8 127.0.0.0/8 169.254.0.0/16 	\
		   172.16.0.0/12 192.0.0.0/24 192.0.2.0/24 224.0.0.0/3  \
		   192.168.0.0/16 192.18.0.0/15 198.51.100.0/24 	\
		   203.0.113.0/24 !192.168.0.0/24 !10.1.1.0/24 		\
		   !10.2.2.0/24 !10.3.3.0/24 }

# ------------------------------------ #
# --- Options to be set at runtime --- #
# ------------------------------------ #
# Do not filter loopback
set skip on $lo_if

# Set global block-policy to drop packets silently.
set block-policy drop

# Enables the collection of packet and byte count statistics for a given interface or interface group.
set loginterface egress

# Enables pf to answer all incoming TCP SYN with syncookie SYNACK, making pf resilient against synflood attacks.
set syncookies always

# ----------------------------- #
# --- Traffic normalization --- #	
# ----------------------------- #
# Enables the clean-up of packet content
match in all scrub (no-df random-id max-mss 1440)

# ------------------- #
# --- Perform NAT --- #
# ------------------- #
match out on $main_if inet from ($main_if:network) to any nat-to ($main_if:0)
match out on $main_if inet from ($lan1_if:network) to any nat-to ($main_if:0)

# ------------------------ #
# --- Prevent Spoofing --- #
# ------------------------ #
# Sets protection against spoofing for all interfaces
antispoof quick for { $main_if $lan1_if $lan2_if }
block in from no-route
block in quick from urpf-failed

# Blocks non-routable private addresses as per RFC1918
block in quick on { $main_if } from <martians> to any
block return out quick on { $main_if } from any to <martians>

# Default blocking all incoming traffic on all NICs
block return in on { $main_if $lan1_if $lan2_if }
block drop in log on { $main_if }

# ----------------------- #
# --- Filtering Rules --- #
# ----------------------- #
# Default Deny All
# block all

# Allow all NICs to pass out IPv4 data
pass out log inet

# ICMP
pass in on { $main_if $lan1_if $lan2_if } inet proto icmp icmp-type $icmp_types

# SSH - Allows my workstation to access all devices through SSH
pass in on { $main_if } inet proto tcp from { $void_ws } to { $main_if:network $lan1_if:network $lan2_if:network } port 22

# DNS - Allows the Unbound DNS server to query the forwarders on port 853 and the NSD servers on 53530
pass in log on { $lan1_if } inet proto { udp tcp } from { $un_srv } to { any } port { 853 53530 } 
pass in log on { $main_if } inet proto { udp tcp } from { $main_if:network } to { $un_srv } port 53

# NTP, LDAP, Mail Exchange & HTTPS
pass in log proto { udp tcp } from { $main_if:network $lan1_if:network } to any port { 80 123 389 443 587 993 }
```
You may save the file and it's done. For now, I have no need to set up a rdr rule, but in case I deploy a web server, I could redirect all packets coming from the Internet to ports 80 and 443 directly to my web server with somthing similar to this:
```
pass in on { $main_if } inet proto { tcp } to { $main_if } port { 80 443 } rdr-to $web_server
```
And remember that redirects always comes last in the ruleset.
## Finalising
Now that all the configuration is done, we can test if the pf.conf is healthy with [pfctl(8)](https://man.openbsd.org/pfctl.8):
```
# pfctl -nf /etc/pf.conf
```
If there are no syntax errors, then we can load the ruleset by simply removing the -n option:
```
# pfctl -f /etc/pf.conf
```
When pf loads the configuration file, the rules we've applied are automatically expanded. We can see this with:
```
# pfctl -s rules
```
## Logging
With pf running with no errors, it is possible that something went not as you expected and you may have to tweak your ruleset, or maybe you want to monitor what's happening in your network.
You can check the logs using [tcpdump(8)](https://man.openbsd.org/tcpdump.8), and it provides us many choices in what we want to see.

We can check the log with:
```
# tcpdump -n -e -ttt -r /var/log/pflog
```
In case you want to monitor pf in real time for an specific host, you can monitor the pseudo-network interface - [pflog(4)](https://man.openbsd.org/man4/pflog.4), for example:
```
# tcpdump -n -e -ttt -i pflog0 host 10.1.1.127
```
This allows us to see in real time what's happening with the host 10.1.1.127:
```
Nov 24 17:26:43.369487 rule 23/(match) pass in on em0: 192.168.0.34.59398 > 10.1.1.127.22: S 898656153:898656153(0) win 8 <mss 1460> (DF) [tos 0x10]
Nov 24 17:26:43.369512 rule 25/(match) pass out on em1: 192.168.0.34.59398 > 10.1.1.127.22: S 2003690891:2003690891(0) win 0 (DF) [tos 0x10]
Nov 24 17:26:58.549096 rule 58/(match) pass in on em1: 10.1.1.127.43292 > 91.239.100.100.853: S 3698582108:3698582108(0) win 8 <mss 1460> (DF) [tos 0x10]
Nov 24 17:26:59.131803 rule 58/(match) pass in on em1: 10.1.1.127.5162 > 91.239.100.100.853: S 2475399542:2475399542(0) win 8 <mss 1460> (DF) [tos 0x10]
Nov 24 17:26:59.732385 rule 58/(match) pass in on em1: 10.1.1.127.10986 > 176.9.93.198.853: S 2007924624:2007924624(0) win 8 <mss 1460> (DF) [tos 0x10]
Nov 24 17:27:02.173350 rule 58/(match) pass in on em1: 10.1.1.127.29893 > 176.9.93.198.853: S 3642148562:3642148562(0) win 8 <mss 1460> (DF) [tos 0x10]
```
And with that you should have your router working fine, as well as be able to monitor and change your ruleset as you see fit.

## Final considerations
If you have any comments, corrections or suggestions you consider appropriate, please feel free to contact me.
